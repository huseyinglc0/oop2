﻿
namespace OnlineStoreApp
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.siticoneShadowForm1 = new Siticone.Desktop.UI.WinForms.SiticoneShadowForm(this.components);
            this.siticonePanel1 = new Siticone.Desktop.UI.WinForms.SiticonePanel();
            this.btn_logout = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.btn_admin = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.btn_cart = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.btn_store = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.siticonePanel2 = new Siticone.Desktop.UI.WinForms.SiticonePanel();
            this.siticoneControlBox2 = new Siticone.Desktop.UI.WinForms.SiticoneControlBox();
            this.siticoneControlBox1 = new Siticone.Desktop.UI.WinForms.SiticoneControlBox();
            this.siticoneDragControl1 = new Siticone.Desktop.UI.WinForms.SiticoneDragControl(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_add_img = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_update = new System.Windows.Forms.Button();
            this.txt_price = new System.Windows.Forms.TextBox();
            this.txt_description = new System.Windows.Forms.TextBox();
            this.dataGridStore = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbl_tutar = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.upDown_adet = new System.Windows.Forms.NumericUpDown();
            this.btn_onay = new System.Windows.Forms.Button();
            this.btn_back_Shapping = new System.Windows.Forms.Button();
            this.GridShopCart = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btn_backStore = new System.Windows.Forms.Button();
            this.btn_addCard = new System.Windows.Forms.Button();
            this.label_productDescription = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label_productPrice = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label_productName = new System.Windows.Forms.Label();
            this.label_product = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ımageList1 = new System.Windows.Forms.ImageList(this.components);
            this.siticonePanel1.SuspendLayout();
            this.siticonePanel2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStore)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_adet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridShopCart)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // siticonePanel1
            // 
            this.siticonePanel1.Controls.Add(this.btn_logout);
            this.siticonePanel1.Controls.Add(this.btn_admin);
            this.siticonePanel1.Controls.Add(this.btn_cart);
            this.siticonePanel1.Controls.Add(this.btn_store);
            this.siticonePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.siticonePanel1.FillColor = System.Drawing.Color.White;
            this.siticonePanel1.Location = new System.Drawing.Point(0, 0);
            this.siticonePanel1.Name = "siticonePanel1";
            this.siticonePanel1.ShadowDecoration.Depth = 5;
            this.siticonePanel1.ShadowDecoration.Enabled = true;
            this.siticonePanel1.ShadowDecoration.Parent = this.siticonePanel1;
            this.siticonePanel1.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(3);
            this.siticonePanel1.Size = new System.Drawing.Size(192, 645);
            this.siticonePanel1.TabIndex = 0;
            // 
            // btn_logout
            // 
            this.btn_logout.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.btn_logout.CheckedState.CustomBorderColor = System.Drawing.Color.Red;
            this.btn_logout.CheckedState.FillColor = System.Drawing.Color.White;
            this.btn_logout.CheckedState.Image = global::OnlineStoreApp.Properties.Resources.icons8_logout_rounded_down_50;
            this.btn_logout.CheckedState.Parent = this.btn_logout;
            this.btn_logout.CustomBorderThickness = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btn_logout.CustomImages.Parent = this.btn_logout;
            this.btn_logout.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_logout.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_logout.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_logout.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_logout.DisabledState.Parent = this.btn_logout;
            this.btn_logout.FillColor = System.Drawing.Color.White;
            this.btn_logout.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_logout.ForeColor = System.Drawing.Color.Black;
            this.btn_logout.HoverState.Parent = this.btn_logout;
            this.btn_logout.Image = global::OnlineStoreApp.Properties.Resources.icons8_logout_rounded_down_50__1_;
            this.btn_logout.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_logout.ImageOffset = new System.Drawing.Point(15, 0);
            this.btn_logout.ImageSize = new System.Drawing.Size(30, 30);
            this.btn_logout.Location = new System.Drawing.Point(-1, 567);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.PressedDepth = 0;
            this.btn_logout.ShadowDecoration.Parent = this.btn_logout;
            this.btn_logout.Size = new System.Drawing.Size(192, 45);
            this.btn_logout.TabIndex = 3;
            this.btn_logout.Text = "LOGOUT";
            this.btn_logout.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_logout.TextOffset = new System.Drawing.Point(15, 0);
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_admin
            // 
            this.btn_admin.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.btn_admin.CheckedState.CustomBorderColor = System.Drawing.Color.Red;
            this.btn_admin.CheckedState.FillColor = System.Drawing.Color.White;
            this.btn_admin.CheckedState.Image = global::OnlineStoreApp.Properties.Resources.icons8_admin_settings_male_50__1_;
            this.btn_admin.CheckedState.Parent = this.btn_admin;
            this.btn_admin.CustomBorderThickness = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btn_admin.CustomImages.Parent = this.btn_admin;
            this.btn_admin.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_admin.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_admin.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_admin.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_admin.DisabledState.Parent = this.btn_admin;
            this.btn_admin.FillColor = System.Drawing.Color.White;
            this.btn_admin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_admin.ForeColor = System.Drawing.Color.Black;
            this.btn_admin.HoverState.Parent = this.btn_admin;
            this.btn_admin.Image = global::OnlineStoreApp.Properties.Resources.icons8_admin_settings_male_50__2_;
            this.btn_admin.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_admin.ImageOffset = new System.Drawing.Point(15, 0);
            this.btn_admin.ImageSize = new System.Drawing.Size(30, 30);
            this.btn_admin.Location = new System.Drawing.Point(0, 273);
            this.btn_admin.Name = "btn_admin";
            this.btn_admin.PressedDepth = 0;
            this.btn_admin.ShadowDecoration.Parent = this.btn_admin;
            this.btn_admin.Size = new System.Drawing.Size(192, 45);
            this.btn_admin.TabIndex = 2;
            this.btn_admin.Text = "ADMIN";
            this.btn_admin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_admin.TextOffset = new System.Drawing.Point(15, 0);
            this.btn_admin.CheckedChanged += new System.EventHandler(this.btn_admin_CheckedChanged);
            this.btn_admin.Click += new System.EventHandler(this.btn_admin_Click);
            // 
            // btn_cart
            // 
            this.btn_cart.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.btn_cart.CheckedState.CustomBorderColor = System.Drawing.Color.Red;
            this.btn_cart.CheckedState.FillColor = System.Drawing.Color.White;
            this.btn_cart.CheckedState.Image = global::OnlineStoreApp.Properties.Resources.icons8_fast_cart_64__1_;
            this.btn_cart.CheckedState.Parent = this.btn_cart;
            this.btn_cart.CustomBorderThickness = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btn_cart.CustomImages.Parent = this.btn_cart;
            this.btn_cart.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_cart.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_cart.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_cart.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_cart.DisabledState.Parent = this.btn_cart;
            this.btn_cart.FillColor = System.Drawing.Color.White;
            this.btn_cart.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_cart.ForeColor = System.Drawing.Color.Black;
            this.btn_cart.HoverState.Parent = this.btn_cart;
            this.btn_cart.Image = global::OnlineStoreApp.Properties.Resources.icons8_fast_cart_64__2_;
            this.btn_cart.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_cart.ImageOffset = new System.Drawing.Point(15, 0);
            this.btn_cart.ImageSize = new System.Drawing.Size(30, 30);
            this.btn_cart.Location = new System.Drawing.Point(-1, 222);
            this.btn_cart.Name = "btn_cart";
            this.btn_cart.PressedDepth = 0;
            this.btn_cart.ShadowDecoration.Parent = this.btn_cart;
            this.btn_cart.Size = new System.Drawing.Size(192, 45);
            this.btn_cart.TabIndex = 1;
            this.btn_cart.Text = "CART";
            this.btn_cart.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_cart.TextOffset = new System.Drawing.Point(15, 0);
            this.btn_cart.CheckedChanged += new System.EventHandler(this.btn_cart_CheckedChanged);
            // 
            // btn_store
            // 
            this.btn_store.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.btn_store.Checked = true;
            this.btn_store.CheckedState.CustomBorderColor = System.Drawing.Color.Red;
            this.btn_store.CheckedState.FillColor = System.Drawing.Color.White;
            this.btn_store.CheckedState.Image = global::OnlineStoreApp.Properties.Resources.icons8_grocery_store_50__1_;
            this.btn_store.CheckedState.Parent = this.btn_store;
            this.btn_store.CustomBorderThickness = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btn_store.CustomImages.Parent = this.btn_store;
            this.btn_store.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_store.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_store.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_store.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_store.DisabledState.Parent = this.btn_store;
            this.btn_store.FillColor = System.Drawing.Color.White;
            this.btn_store.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_store.ForeColor = System.Drawing.Color.Black;
            this.btn_store.HoverState.Parent = this.btn_store;
            this.btn_store.Image = global::OnlineStoreApp.Properties.Resources.icons8_grocery_store_50__2_;
            this.btn_store.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_store.ImageOffset = new System.Drawing.Point(15, 0);
            this.btn_store.ImageSize = new System.Drawing.Size(30, 30);
            this.btn_store.Location = new System.Drawing.Point(0, 171);
            this.btn_store.Name = "btn_store";
            this.btn_store.PressedDepth = 0;
            this.btn_store.ShadowDecoration.Parent = this.btn_store;
            this.btn_store.Size = new System.Drawing.Size(192, 45);
            this.btn_store.TabIndex = 0;
            this.btn_store.Text = "STORE";
            this.btn_store.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_store.TextOffset = new System.Drawing.Point(15, 0);
            this.btn_store.CheckedChanged += new System.EventHandler(this.siticoneButton1_CheckedChanged);
            // 
            // siticonePanel2
            // 
            this.siticonePanel2.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.siticonePanel2.AutoRoundedCorners = true;
            this.siticonePanel2.BackColor = System.Drawing.Color.White;
            this.siticonePanel2.BorderRadius = 25;
            this.siticonePanel2.Controls.Add(this.siticoneControlBox2);
            this.siticonePanel2.Controls.Add(this.siticoneControlBox1);
            this.siticonePanel2.Location = new System.Drawing.Point(196, 0);
            this.siticonePanel2.Name = "siticonePanel2";
            this.siticonePanel2.ShadowDecoration.Parent = this.siticonePanel2;
            this.siticonePanel2.Size = new System.Drawing.Size(1001, 52);
            this.siticonePanel2.TabIndex = 1;
            // 
            // siticoneControlBox2
            // 
            this.siticoneControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.siticoneControlBox2.ControlBoxType = Siticone.Desktop.UI.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.siticoneControlBox2.FillColor = System.Drawing.Color.White;
            this.siticoneControlBox2.HoverState.Parent = this.siticoneControlBox2;
            this.siticoneControlBox2.IconColor = System.Drawing.Color.Black;
            this.siticoneControlBox2.Location = new System.Drawing.Point(905, 0);
            this.siticoneControlBox2.Name = "siticoneControlBox2";
            this.siticoneControlBox2.ShadowDecoration.Parent = this.siticoneControlBox2;
            this.siticoneControlBox2.Size = new System.Drawing.Size(53, 52);
            this.siticoneControlBox2.TabIndex = 1;
            // 
            // siticoneControlBox1
            // 
            this.siticoneControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.siticoneControlBox1.FillColor = System.Drawing.Color.White;
            this.siticoneControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.siticoneControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.siticoneControlBox1.HoverState.Parent = this.siticoneControlBox1;
            this.siticoneControlBox1.IconColor = System.Drawing.Color.Black;
            this.siticoneControlBox1.Location = new System.Drawing.Point(951, 0);
            this.siticoneControlBox1.Name = "siticoneControlBox1";
            this.siticoneControlBox1.ShadowDecoration.Parent = this.siticoneControlBox1;
            this.siticoneControlBox1.Size = new System.Drawing.Size(53, 52);
            this.siticoneControlBox1.TabIndex = 0;
            // 
            // siticoneDragControl1
            // 
            this.siticoneDragControl1.TargetControl = this.siticonePanel2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(366, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(242, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Please click on the product you want.";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.btn_add_img);
            this.tabPage1.Controls.Add(this.btn_add);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txt_name);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btn_delete);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btn_update);
            this.tabPage1.Controls.Add(this.txt_price);
            this.tabPage1.Controls.Add(this.txt_description);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(997, 595);
            this.tabPage1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.Color.Black;
            this.dataGridView1.Location = new System.Drawing.Point(7, 153);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(944, 413);
            this.dataGridView1.TabIndex = 39;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // btn_add_img
            // 
            this.btn_add_img.BackColor = System.Drawing.Color.Yellow;
            this.btn_add_img.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_add_img.ForeColor = System.Drawing.Color.Black;
            this.btn_add_img.Location = new System.Drawing.Point(700, 22);
            this.btn_add_img.Name = "btn_add_img";
            this.btn_add_img.Size = new System.Drawing.Size(105, 34);
            this.btn_add_img.TabIndex = 37;
            this.btn_add_img.Text = "Add Image";
            this.btn_add_img.UseVisualStyleBackColor = false;
            this.btn_add_img.Click += new System.EventHandler(this.btn_add_img_Click);
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Lime;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_add.ForeColor = System.Drawing.Color.Black;
            this.btn_add.Location = new System.Drawing.Point(68, 87);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(87, 34);
            this.btn_add.TabIndex = 28;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(811, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 110);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(233, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 36;
            this.label3.Text = "Price :";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(68, 22);
            this.txt_name.Multiline = true;
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(155, 34);
            this.txt_name.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(438, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 16);
            this.label2.TabIndex = 35;
            this.label2.Text = "Description :";
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Red;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_delete.ForeColor = System.Drawing.Color.Black;
            this.btn_delete.Location = new System.Drawing.Point(338, 87);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(87, 34);
            this.btn_delete.TabIndex = 30;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 34;
            this.label1.Text = "Name :";
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.Yellow;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_update.ForeColor = System.Drawing.Color.Black;
            this.btn_update.Location = new System.Drawing.Point(205, 87);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(87, 34);
            this.btn_update.TabIndex = 31;
            this.btn_update.Text = "UPDATE";
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // txt_price
            // 
            this.txt_price.Location = new System.Drawing.Point(291, 22);
            this.txt_price.Multiline = true;
            this.txt_price.Name = "txt_price";
            this.txt_price.Size = new System.Drawing.Size(134, 34);
            this.txt_price.TabIndex = 33;
            // 
            // txt_description
            // 
            this.txt_description.Location = new System.Drawing.Point(539, 17);
            this.txt_description.Multiline = true;
            this.txt_description.Name = "txt_description";
            this.txt_description.Size = new System.Drawing.Size(155, 104);
            this.txt_description.TabIndex = 32;
            // 
            // dataGridStore
            // 
            this.dataGridStore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridStore.Location = new System.Drawing.Point(291, 49);
            this.dataGridStore.MultiSelect = false;
            this.dataGridStore.Name = "dataGridStore";
            this.dataGridStore.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridStore.Size = new System.Drawing.Size(414, 520);
            this.dataGridStore.TabIndex = 0;
            this.dataGridStore.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridStore_MouseClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(192, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1005, 621);
            this.tabControl1.TabIndex = 41;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.dataGridStore);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(997, 595);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbl_tutar);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.upDown_adet);
            this.tabPage3.Controls.Add(this.btn_onay);
            this.tabPage3.Controls.Add(this.btn_back_Shapping);
            this.tabPage3.Controls.Add(this.GridShopCart);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(997, 595);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbl_tutar
            // 
            this.lbl_tutar.AutoSize = true;
            this.lbl_tutar.Location = new System.Drawing.Point(339, 292);
            this.lbl_tutar.Name = "lbl_tutar";
            this.lbl_tutar.Size = new System.Drawing.Size(13, 13);
            this.lbl_tutar.TabIndex = 13;
            this.lbl_tutar.Text = "_";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(262, 292);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Sepet Tutarı :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 292);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Ürün Adedi";
            // 
            // upDown_adet
            // 
            this.upDown_adet.Location = new System.Drawing.Point(168, 285);
            this.upDown_adet.Name = "upDown_adet";
            this.upDown_adet.Size = new System.Drawing.Size(47, 20);
            this.upDown_adet.TabIndex = 10;
            this.upDown_adet.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.upDown_adet.ValueChanged += new System.EventHandler(this.upDown_adet_ValueChanged);
            // 
            // btn_onay
            // 
            this.btn_onay.Location = new System.Drawing.Point(419, 269);
            this.btn_onay.Name = "btn_onay";
            this.btn_onay.Size = new System.Drawing.Size(87, 48);
            this.btn_onay.TabIndex = 9;
            this.btn_onay.Text = "Remove From Cart";
            this.btn_onay.UseVisualStyleBackColor = true;
            this.btn_onay.Click += new System.EventHandler(this.btn_onay_Click);
            // 
            // btn_back_Shapping
            // 
            this.btn_back_Shapping.Location = new System.Drawing.Point(512, 6);
            this.btn_back_Shapping.Name = "btn_back_Shapping";
            this.btn_back_Shapping.Size = new System.Drawing.Size(91, 48);
            this.btn_back_Shapping.TabIndex = 8;
            this.btn_back_Shapping.Text = "Back Store";
            this.btn_back_Shapping.UseVisualStyleBackColor = true;
            this.btn_back_Shapping.Click += new System.EventHandler(this.btn_back_Shapping_Click);
            // 
            // GridShopCart
            // 
            this.GridShopCart.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.GridShopCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridShopCart.Location = new System.Drawing.Point(19, 6);
            this.GridShopCart.Name = "GridShopCart";
            this.GridShopCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridShopCart.Size = new System.Drawing.Size(487, 257);
            this.GridShopCart.TabIndex = 7;
            this.GridShopCart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GridShopCart_MouseClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btn_backStore);
            this.tabPage4.Controls.Add(this.btn_addCard);
            this.tabPage4.Controls.Add(this.label_productDescription);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.label_productPrice);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.label_productName);
            this.tabPage4.Controls.Add(this.label_product);
            this.tabPage4.Controls.Add(this.pictureBox2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(997, 595);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btn_backStore
            // 
            this.btn_backStore.Location = new System.Drawing.Point(618, 496);
            this.btn_backStore.Name = "btn_backStore";
            this.btn_backStore.Size = new System.Drawing.Size(117, 54);
            this.btn_backStore.TabIndex = 19;
            this.btn_backStore.Text = "Back Store";
            this.btn_backStore.UseVisualStyleBackColor = true;
            this.btn_backStore.Click += new System.EventHandler(this.btn_backStore_Click);
            // 
            // btn_addCard
            // 
            this.btn_addCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_addCard.Location = new System.Drawing.Point(431, 496);
            this.btn_addCard.Name = "btn_addCard";
            this.btn_addCard.Size = new System.Drawing.Size(117, 54);
            this.btn_addCard.TabIndex = 10;
            this.btn_addCard.Text = "Add To Card";
            this.btn_addCard.UseVisualStyleBackColor = true;
            this.btn_addCard.Click += new System.EventHandler(this.btn_addCard_Click);
            // 
            // label_productDescription
            // 
            this.label_productDescription.AutoSize = true;
            this.label_productDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productDescription.Location = new System.Drawing.Point(466, 447);
            this.label_productDescription.Name = "label_productDescription";
            this.label_productDescription.Size = new System.Drawing.Size(0, 20);
            this.label_productDescription.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(324, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Description :";
            // 
            // label_productPrice
            // 
            this.label_productPrice.AutoSize = true;
            this.label_productPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productPrice.Location = new System.Drawing.Point(466, 417);
            this.label_productPrice.Name = "label_productPrice";
            this.label_productPrice.Size = new System.Drawing.Size(0, 20);
            this.label_productPrice.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(375, 417);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "Price :";
            // 
            // label_productName
            // 
            this.label_productName.AutoSize = true;
            this.label_productName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_productName.Location = new System.Drawing.Point(466, 387);
            this.label_productName.Name = "label_productName";
            this.label_productName.Size = new System.Drawing.Size(0, 20);
            this.label_productName.TabIndex = 14;
            // 
            // label_product
            // 
            this.label_product.AutoSize = true;
            this.label_product.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_product.Location = new System.Drawing.Point(369, 387);
            this.label_product.Name = "label_product";
            this.label_product.Size = new System.Drawing.Size(65, 20);
            this.label_product.TabIndex = 13;
            this.label_product.Text = "Name :";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(351, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(316, 356);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ımageList1
            // 
            this.ımageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ımageList1.ImageStream")));
            this.ımageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ımageList1.Images.SetKeyName(0, "CocaCola.jpg");
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 645);
            this.Controls.Add(this.siticonePanel2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.siticonePanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.siticonePanel1.ResumeLayout(false);
            this.siticonePanel2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStore)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_adet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridShopCart)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Siticone.Desktop.UI.WinForms.SiticoneShadowForm siticoneShadowForm1;
        private Siticone.Desktop.UI.WinForms.SiticonePanel siticonePanel1;
        private Siticone.Desktop.UI.WinForms.SiticoneButton btn_logout;
        private Siticone.Desktop.UI.WinForms.SiticoneButton btn_admin;
        private Siticone.Desktop.UI.WinForms.SiticoneButton btn_cart;
        private Siticone.Desktop.UI.WinForms.SiticoneButton btn_store;
        private Siticone.Desktop.UI.WinForms.SiticoneDragControl siticoneDragControl1;
        private Siticone.Desktop.UI.WinForms.SiticoneControlBox siticoneControlBox2;
        private Siticone.Desktop.UI.WinForms.SiticoneControlBox siticoneControlBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_add_img;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.TextBox txt_price;
        private System.Windows.Forms.TextBox txt_description;
        private System.Windows.Forms.DataGridView dataGridStore;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ImageList ımageList1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lbl_tutar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown upDown_adet;
        private System.Windows.Forms.Button btn_onay;
        private System.Windows.Forms.Button btn_back_Shapping;
        private System.Windows.Forms.DataGridView GridShopCart;
        private Siticone.Desktop.UI.WinForms.SiticonePanel siticonePanel2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btn_addCard;
        private System.Windows.Forms.Label label_productDescription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label_productPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_productName;
        private System.Windows.Forms.Label label_product;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btn_backStore;
    }
}