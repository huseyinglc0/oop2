﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineStoreApp
{
    public partial class Main : Form
    {
        int indexRow;
        static List<Product> list = new List<Product>();
        int adminOrUser;
        

        public Main(int count)
        {
            InitializeComponent();
            adminOrUser = count;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            siticoneShadowForm1.SetShadowForm(this);

            DataGridViewImageColumn dgvImage = new DataGridViewImageColumn();
            dgvImage.HeaderText = "Image";
            dgvImage.ImageLayout = DataGridViewImageCellLayout.Stretch;

            DataGridViewTextBoxColumn dgvName = new DataGridViewTextBoxColumn();
            dgvName.HeaderText = "Name";

            DataGridViewTextBoxColumn dgvPrice = new DataGridViewTextBoxColumn();
            dgvPrice.HeaderText = "Price";

            DataGridViewTextBoxColumn dgvDescription = new DataGridViewTextBoxColumn();
            dgvDescription.HeaderText = "Description";

            //Store tab için grid başlıkları ve ayarları

            DataGridViewImageColumn dgvImage2 = new DataGridViewImageColumn();
            dgvImage2.HeaderText = "Products";
            dgvImage2.ImageLayout = DataGridViewImageCellLayout.Stretch;

            DataGridViewTextBoxColumn dgvName2 = new DataGridViewTextBoxColumn();
            dgvName2.HeaderText = "Name";

            DataGridViewTextBoxColumn dgvPrice2 = new DataGridViewTextBoxColumn();
            dgvPrice2.HeaderText = "Price";

            DataGridViewTextBoxColumn dgvDescription2 = new DataGridViewTextBoxColumn();
            dgvDescription2.HeaderText = "Description";



            dataGridView1.Columns.Add(dgvName);
            dataGridView1.Columns.Add(dgvPrice);
            dataGridView1.Columns.Add(dgvDescription);
            dataGridView1.Columns.Add(dgvImage);

            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.RowTemplate.Height = 150;

            dataGridView1.AllowUserToAddRows = false;

            dataGridStore.Columns.Add(dgvName2);
            dataGridStore.Columns.Add(dgvPrice2);
            dataGridStore.Columns.Add(dgvDescription2);
            dataGridStore.Columns.Add(dgvImage2);

            dataGridStore.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridStore.RowTemplate.Height = 250;

            dataGridStore.Columns[0].Visible = false;
            dataGridStore.Columns[1].Visible = false;
            dataGridStore.Columns[2].Visible = false;

            dataGridStore.AllowUserToAddRows = false;

            GridShopCart.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            GridShopCart.RowTemplate.Height = 50;

            GridShopCart.AllowUserToAddRows = false;

            DataGridViewTextBoxColumn dgvName3 = new DataGridViewTextBoxColumn();
            dgvName3.HeaderText = "Name";

            DataGridViewTextBoxColumn dgvPrice3 = new DataGridViewTextBoxColumn();
            dgvPrice3.HeaderText = "Price";

            DataGridViewTextBoxColumn dgvamount3 = new DataGridViewTextBoxColumn();
            dgvamount3.HeaderText = "Amount";

            GridShopCart.Columns.Add(dgvName3);
            GridShopCart.Columns.Add(dgvPrice3);
            GridShopCart.Columns.Add(dgvamount3);


            Image img;
            Image img2;

            string[] images = new string[10] { "CocaCola", "fanta", "icetea", "eker", "dimes", "kefir", "kipa", "uludag", "süt", "soda" };


            Drink cola = new Drink() { Name = "Coca-Cola", Price = "5.25", Desciription = "Coca-Cola 1.5 L " };


            for (int i = 0; i < 10; i++)
            {

                img = Image.FromFile(@"C:\images\" + images[i] + ".jpg");
                img2 = Image.FromFile(@"C:\images\" + images[i] + ".jpg");

                switch (i)
                {
                    case 0:
                        dataGridView1.Rows.Add("Coca-Cola", "5,25", "Coca-Cola 1.5 L ", img);
                        dataGridStore.Rows.Add("Coca-Cola", "5,25", "Coca-Cola 1.5 L ", img);
                        break;
                    case 1:
                        dataGridView1.Rows.Add("Fanta", "5,25", "Fanta 1.5 L", img);
                        dataGridStore.Rows.Add("Fanta", "5,25", "Fanta 1.5 L", img);
                        break;
                    case 2:
                        dataGridView1.Rows.Add("Lipton Ice Tea", "7,00", "Lipton Ice Tea Şeftali Pet 1.5 L", img);
                        dataGridStore.Rows.Add("Lipton Ice Tea", "7,00", "Lipton Ice Tea Şeftali Pet 1.5 L", img);
                        break;
                    case 3:
                        dataGridView1.Rows.Add("Eker Ayran", "9,00", "Eker Ayran 1.5 L", img);
                        dataGridStore.Rows.Add("Eker Ayran", "9,00", "Eker Ayran 1.5 L", img);
                        break;
                    case 4:
                        dataGridView1.Rows.Add("Dimes", "6,50", "Dimes Karışık Meyve Suyu 1 L", img);
                        dataGridStore.Rows.Add("Dimes", "6,50", "Dimes Karışık Meyve Suyu 1 L", img);
                        break;
                    case 5:
                        dataGridView1.Rows.Add("Altınkılıç Kefir", "7,95", "Altınkılıç Kefir 1 L ", img);
                        dataGridStore.Rows.Add("Altınkılıç Kefir", "7,95", "Altınkılıç Kefir 1 L ", img);
                        break;
                    case 6:
                        dataGridView1.Rows.Add("Kipa Su", "8,20", "Kipa Su 10 L", img);
                        dataGridStore.Rows.Add("Kipa Su", "8,20", "Kipa Su 10 L", img);
                        break;
                    case 7:
                        dataGridView1.Rows.Add("Uludağ Limonata", "5,25", "Uludağ Limonata Şekersiz 1 L", img);
                        dataGridStore.Rows.Add("Uludağ Limonata", "5,25", "Uludağ Limonata Şekersiz 1 L", img);
                        break;
                    case 8:
                        dataGridView1.Rows.Add("İçim Süt", "5,00", "İçim Süt 1 L", img);
                        dataGridStore.Rows.Add("İçim Süt", "5,00", "İçim Süt 1 L", img);
                        break;
                    case 9:
                        dataGridView1.Rows.Add("Damla", "2,00", "Damla Mineral Suyu 0.5 L", img);
                        dataGridStore.Rows.Add("Damla", "2,00", "Damla Mineral Suyu 0.5 L", img);
                        break;


                }


                if (adminOrUser == 0)
                {
                    btn_admin.Hide();
                }

                tabControl1.SelectTab(tabPage2);
            }
        }
        private static List<Product> getList(string name, string price, string description)
        {
            list.Add(new Drink() { Name = name, Price = price, Desciription = description });

            return list;
        }

        private void textClear()
        {
            txt_description.Text = "";
            txt_name.Text = "";
            txt_price.Text = "";
        }

        private void addImage()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Choose Image(*.jpg;*.png;*.gif)|*.jpg;*.png;*.gif";

            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(open.FileName);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];


            if (dataGridView1.CurrentRow.Cells[3].Value.GetType() == typeof(byte[]))
            {
                byte[] imgData = (byte[])dataGridView1.CurrentRow.Cells[3].Value;

                MemoryStream ms = new MemoryStream(imgData);
                pictureBox1.Image = Image.FromStream(ms);

                txt_name.Text = row.Cells[0].Value.ToString();
                txt_price.Text = row.Cells[1].Value.ToString();
                txt_description.Text = row.Cells[2].Value.ToString();
            }
            else
            {
                Image img = (Image)dataGridView1.CurrentRow.Cells[3].Value;
                byte[] imgData = ImageToByteArray(img);
                MemoryStream ms = new MemoryStream(imgData);
                pictureBox1.Image = Image.FromStream(ms);

                txt_name.Text = row.Cells[0].Value.ToString();
                txt_price.Text = row.Cells[1].Value.ToString();
                txt_description.Text = row.Cells[2].Value.ToString();
            }
        }

        private void btn_add_img_Click(object sender, EventArgs e)
        {
            addImage();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure To Delete ?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int rowIndex = dataGridView1.CurrentCell.RowIndex;
                dataGridView1.Rows.RemoveAt(rowIndex);
                dataGridStore.Rows.RemoveAt(rowIndex);

                textClear();
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (txt_name.Text != "" && txt_price.Text != "" && txt_description.Text != "")
            {
                if (MessageBox.Show("Are You Sure To Update ?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MemoryStream ms = new MemoryStream();
                    DataGridViewRow newDataRow = dataGridView1.Rows[indexRow];
                    MemoryStream ms1 = new MemoryStream();
                    DataGridViewRow newDataRowStore = dataGridStore.Rows[indexRow];


                    try
                    {
                        newDataRow.Cells[0].Value = txt_name.Text;
                        newDataRow.Cells[1].Value = txt_price.Text;
                        newDataRow.Cells[2].Value = txt_description.Text;

                        pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                        byte[] img = ms.ToArray();
                        dataGridView1.CurrentRow.Cells[3].Value = img;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }

                    newDataRowStore.Cells[0].Value = txt_name.Text;
                    newDataRowStore.Cells[1].Value = txt_price.Text;
                    newDataRowStore.Cells[2].Value = txt_description.Text;


                }


            }

            else
            {
                MessageBox.Show("Lütfen Boş Alan Bırakmayınız!!", "Boş Alan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            textClear();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (txt_name.Text != "" && txt_price.Text != "" && txt_description.Text != "" && pictureBox1.Image != null)
            {
                string name = txt_name.Text;
                string price = txt_price.Text;
                string description = txt_description.Text;

                if (MessageBox.Show("Are You Sure To Add ?", "Add Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {

                        MemoryStream ms = new MemoryStream();
                        pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                        byte[] img = ms.ToArray();

                        dataGridView1.Rows.Add(name, price, description, img);
                        dataGridStore.Rows.Add(name, price, description, img);
                        getList(name, price, description);



                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                }




            }

            else
            {
                MessageBox.Show("Lütfen Boş Alan Bırakmayınız!!", "Boş Alan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            textClear();
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        private void dataGridStore_MouseClick(object sender, MouseEventArgs e)
        {
               
            pictureBox2.Image = (Image)dataGridStore.SelectedRows[0].Cells[3].Value;
            label_productName.Text = dataGridStore.SelectedRows[0].Cells[0].Value.ToString();
            label_productPrice.Text = "$ " + dataGridStore.SelectedRows[0].Cells[1].Value.ToString();
            label_productDescription.Text = dataGridStore.SelectedRows[0].Cells[2].Value.ToString();


            tabControl1.SelectTab(tabPage4);


        }

        private void siticoneButton1_CheckedChanged(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage2);
           
        }

        private void btn_cart_CheckedChanged(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage3);
        }

        private void btn_admin_CheckedChanged(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage1);
        }

        private void btn_backStore_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage2);
        }

        static string nameCart, priceCart;
        double tutar2 = 0;
   


        private void btn_addCard_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to add the product?", "Add Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    int count = GridShopCart.RowCount;

                    nameCart = dataGridStore.SelectedRows[0].Cells[0].Value.ToString();
                    priceCart = dataGridStore.SelectedRows[0].Cells[1].Value.ToString();


                    GridShopCart.Rows.Add(nameCart, priceCart, upDown_adet.Value);

                    tutar2 += Double.Parse(priceCart) * Double.Parse(upDown_adet.Value.ToString());

                    lbl_tutar.Text = String.Format("{0:0.00}", tutar2.ToString("C"));



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }


           
        }

        private void btn_back_Shapping_Click(object sender, EventArgs e)
        {
            btn_store.Checked=true;
           
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginForm login = new LoginForm();
            login.Show();
        }

        private void GridShopCart_MouseClick(object sender, MouseEventArgs e)
        {
            upDown_adet.Value = (decimal)GridShopCart.SelectedRows[0].Cells[2].Value;
        }

        private void btn_onay_Click(object sender, EventArgs e)
        {
            int rowIndex2 = GridShopCart.CurrentCell.RowIndex;
            int removeCount = Int32.Parse(GridShopCart.SelectedRows[0].Cells[2].Value.ToString());
            double removePrice= Double.Parse(GridShopCart.SelectedRows[0].Cells[1].Value.ToString());
            GridShopCart.Rows.RemoveAt(rowIndex2);

            tutar2 -= removePrice * removeCount;

            lbl_tutar.Text = String.Format("{0:0.00}", tutar2.ToString("C"));
        }

        private void btn_admin_Click(object sender, EventArgs e)
        {

        }

        private void upDown_adet_ValueChanged(object sender, EventArgs e)
        {
           
            int x = Int32.Parse(GridShopCart.SelectedRows[0].Cells[2].Value.ToString());
            GridShopCart.SelectedRows[0].Cells[2].Value = upDown_adet.Value;


            tutar2 += Double.Parse(priceCart) * ((int)upDown_adet.Value-x);


            lbl_tutar.Text = String.Format("{0:0.00}", tutar2.ToString("C"));
        }
    }
}
