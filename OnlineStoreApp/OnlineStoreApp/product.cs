﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStoreApp
{
    public abstract class Product
    {            
        public string Name { get; set; }
        public string Price { get; set; }
        public string Desciription { get; set; }
        public string PhotoUrl { get; set; }
        public long Id { get; set; }
    }
}

