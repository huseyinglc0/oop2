﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineStoreApp
{
    public partial class LoginForm : Form

    {
        Main newMain;
        public string adminUserName = "admin";
        public string adminPassword = "admin";

        public string userName;
        public string password;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage2);
                 
        }

    

        private void signUpBtn_Click(object sender, EventArgs e)
        {
            if ( txt_password.Text!="" && txt_passwordAgain.Text!="" && txt_username.Text != "")
            {
                if(txt_password.Text == txt_passwordAgain.Text)
                {
                    var newUser = new Login(txt_username.Text, txt_password.Text);
                    userName = newUser.Username;
                    password = newUser.Password;

                    MessageBox.Show("Sign up Successful", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    tabControl1.SelectTab(tabPage2);

                   
                }

                else
                {
                    MessageBox.Show("Girilen Parolalar Uyuşmuyor.", "Parola hatası", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               

            }

            else
            {
                MessageBox.Show("Lütfen Boş Alan bırakmayınız...", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txt_username_signin.Text == adminUserName && txt_password_signin.Text == adminPassword)
            {
                MessageBox.Show("Admin Logged In", "Welcome Admin", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txt_username.Text = "";
                txt_password.Text = "";

                newMain = new Main(1);
                newMain.Show();
                this.Hide();

            }
            else if (txt_username_signin.Text == userName && txt_password_signin.Text == password)
            {
                MessageBox.Show("User Logged In", "Welcome User", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                txt_username.Text = "";
                txt_password.Text = "";

                newMain = new Main(0);
                newMain.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Unsuccessful Login", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txt_username.Text = "";
                txt_password.Text = "";

            }
            
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(tabPage1);
        }

    }
}
