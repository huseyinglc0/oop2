﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStoreApp
{
    //Factory Method Pattern
    class ProductFactory
    {
        /*FactoryMethod kullanımı:
            ->(....., true) ise yeni bir ürün oluşturmaz ekranda 
                ürünlerin görüntülenmesi için kullanılır. (Anasayfada ürünleri listelenmesi LoadCsv)

            ->(....., false) ise yeni ürün oluştur. (Add button click event'i)
            private void btnAddDrink_Click
            {
                Drink newDrink = (Drink)ProductFactory.FactoryMethod("Drink", txtName, .... false);
                .....
                ....
                List.Add(newDrink);
            }
            
             */

        /* Şuan sadece içecek kategorisi var ileride artacağını düşünürek oluşturulmuştur. Seçim yapmak istediğimizde 
            ilgili kategoride sınıf oluşturmasını sağlar
            
             */
       
        static public Product FactoryMethod(string choice, string name, string price, string desciription, bool bl)
        {
            long ID = (bl) ? (int.Parse(GetID())) : 0;//unique ID

            Product objChosen = null;

            switch (choice)
            {
                case "Drink":
                    objChosen = new Drink(name, price, desciription);
                    break;
            }

            

            return objChosen;

        }

        public static string GetID()
        {
            string uniqueID = Guid.NewGuid().ToString();
            string numericUniqueID = string.Empty;
            foreach (char item in uniqueID)
            {
                if (char.IsNumber(item))
                {
                    numericUniqueID += item;
                }
            }
            return numericUniqueID.Substring(0, 4);
        }
    }
}
